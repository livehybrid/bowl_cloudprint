<?php

require_once 'Config.php';

require_once 'GoogleCloudPrint.php';

session_start();
// Create object

$gcp = new GoogleCloudPrint();

$refreshTokenConfig['refresh_token'] = $_GET['refresh_token'];

$token = $gcp->getAccessTokenByRefreshToken($urlconfig['refreshtoken_url'],http_build_query($refreshTokenConfig));

$gcp->setAuthToken($token);
if (!isset($_REQUEST['printerID'])) {

	$printers = $gcp->getPrinters();
	//print_r($printers);
	
	$printerid = "";
	if(count($printers)==0) {
		
		echo "Could not get printers";
		exit;
	}
	else {
		print "<ul>";	
		foreach ($printers as $printer) {
			if ($printer['connectionStatus']=="ONLINE") {
				print '<li><a href="'.$_SERVER['PHP_SELF'].'?printerID='.$printer['id'].'">'.$printer['displayName'].'</a> [ONLINE]</li>';
			} else {
				print "<li>{$printer['displayName']} [OFFLINE]</li>";
			}
		}
		// Send document to the printer
		//$resarray = $gcp->sendPrintToPrinter($printerid, "Printing Doc using Google Cloud Printing", "./pdf.pdf", "application/pdf");
		
	
		print "</ul>";
	}
} else {
	
	if (!isset($_FILES['uploadFile'])) {
		?>
		<h3>Select file to send to printer</h3>
		<form action="<?php print $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="printerID" value="<?=$_GET['printerID'];?>" />
			<input type="file" name="uploadFile" /><br />
			<input type="submit" value="Send file" />
		</form>
		<?php
	} else {
	//	die("Send file to printer");
	var_dump($_FILES);
		$uploadedFileName = time() . "_" . $_FILES['uploadFile']['name'];
		if (move_uploaded_file($_FILES['uploadFile']['tmp_name'], dirname(__FILE__)."/" . $uploadedFileName)) {
		  //echo "File is valid, and was successfully uploaded.\n";
		  $resarray = $gcp->sendPrintToPrinter($_REQUEST['printerID'], "Printing Doc using Google Cloud Printing", dirname(__FILE__)."/" . $uploadedFileName, "application/pdf");
		} else {
		   echo "Upload failed";
		}
	}
	
	
}
